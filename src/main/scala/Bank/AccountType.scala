package Bank

object AccountType extends Enumeration {
  val SAVINGS  =  Value("Savings")
  val CURRENT = Value("Current")

}

object Mode extends Enumeration{
  val ATM  = Value("ATM")
  val NETBANKNG = Value("NETBANKING")

}

//object Demo{
//  def main(args: Array[String]): Unit = {
//    println(AccountType.SAVINGS)
//    println(AccountType.CURRENT)
//    println(AccountType.SAVINGS.toString == "Savings")
//  }
//}
//


package Bank.Banker

import java.sql.{Connection, DriverManager, Statement}

import Bank.PasswordGenerator

class BankerAuth {

  private val DB_NAME = "USER.db"
  private val CON_STRING  = "jdbc:sqlite://Users/apple/Desktop/scala/BankingSystem/" + DB_NAME
  private val LEDGER_TAB = "ledger"
  private val BANKER_TAB  = "banker"
  private val PASS_SIZE  = 30
  private var name  = ""
  private var connection : Connection = null
  private var statement : Statement = null
  private val generator = new PasswordGenerator
  private var isLogIn = false


  def isLoggedIn:Boolean  = isLogIn

  def logOut = {
    isLogIn =false
    name = ""
  }
  def signUp(name : String, pass : String) = {
    try {
      connection = DriverManager.getConnection(CON_STRING)
      statement =  connection.createStatement()
      connection.setAutoCommit(false)
      statement.execute("CREATE TABLE IF NOT EXISTS "+ BANKER_TAB + "(name text PRIMARY KEY NOT NULL, salt text, rand_str text, password text)")
      connection.commit()
      val salt  =  generator.generateRandomString(PASS_SIZE- pass.length)
      val rand_str =  generator.generateRandomString(PASS_SIZE)

      val newPass = generator.XorString(pass + salt, rand_str)

      statement.execute("INSERT INTO "+ BANKER_TAB + "(name, salt, rand_str, password)"
        + s"VALUES ('$name','$salt','$rand_str','$newPass')")
      connection.commit()
      isLogIn = true
      this.name = name

    }
    catch {
      case e: Exception => println(e.getMessage)
    }
    finally {
      try{
        statement.close()
        connection.close()
      }
      catch {
        case e: Exception => println(e.getMessage)
      }
    }
  }

  def logIn(name: String , pass : String) = {
    isLogIn = validate(name,pass)
    if (isLogIn) {
      this.name = name
      println("Log In Successful !!!.")
    }
    else println("Invalid credential !!!")

  }


  def validate(name:String, pass : String): Boolean = {
    try {
      connection = DriverManager.getConnection(CON_STRING)
      statement = connection.createStatement()
      val result  =  statement.executeQuery("SELECT * FROM "+ BANKER_TAB +
          s" WHERE name = '$name'")
      val banker =  result.getString(1)
      val salt  = result.getString(2)
      val rand_str = result.getString(3)
      val actual_pass  = result.getString(4)
//      println(generator.XorString(actual_pass.take(PASS_SIZE),rand_str))
      val password  = pass + salt
      val newPass  =  generator.XorString(password.take(PASS_SIZE),rand_str)
      if (newPass == actual_pass) true else false
    }
    catch {
      case e: Exception=> println(e.getMessage)
        false
    }
    finally {
      try {
        statement.close()
        connection.close()

      }
      catch {
        case e: Exception => println(e.getMessage)
      }
    }
  }

  def changePassword( newPass : String) = {

    if (isLogIn){
      try {
        connection =  DriverManager.getConnection(CON_STRING)
        statement = connection.createStatement()
        connection.setAutoCommit(false)
       val result  = statement.executeQuery("SELECT * FROM " + BANKER_TAB + s" WHERE name = '${this.name}'")
        val salt = generator.generateRandomString(PASS_SIZE -  newPass.length)
        val rand_str =  result.getString(3)
        val pass  =   newPass + salt
        val password  = generator.XorString(rand_str,pass)
        statement.execute("UPDATE "+ BANKER_TAB + s" SET salt = '$salt' WHERE name  = '${this.name}'")
//        println(salt)
        statement.execute("UPDATE "+ BANKER_TAB + s" SET password = '$password' WHERE name  = '${this.name}'")
        connection.commit()
        println("Password Changed")
        true
      }

      catch {
        case e: Exception => println(e.getMessage)
          false
      }
      finally {
        try{
          statement.close()
          connection.close()
        }
        catch {
          case e : Exception => println(e.getMessage)
        }
      }
    }
    else{
      println("Please Login first")
    }

  }

}

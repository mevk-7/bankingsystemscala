package Bank.Banker

import java.sql.{Connection, DriverManager, Statement}


class BankerDB {

  private val DB_NAME = "USER.db"
  private val CON_STRING  = "jdbc:sqlite://Users/apple/Desktop/scala/BankingSystem/" + DB_NAME
  private val LEDGER_TAB = "ledger"
  private var name  = ""
  private var connection : Connection = null
  private var statement : Statement = null
  private val bankerAuth = new BankerAuth




  def isLoggedIn:Boolean  = bankerAuth.isLoggedIn

  def logOut  =  bankerAuth.logOut

  def login(user : String , password  : String) = {
    bankerAuth.logIn(user,password)
    if (isLoggedIn)
      this.name = name
    isLoggedIn
  }

  def signUp(name : String, pass : String) = {
    bankerAuth.signUp(name,pass)
    if (isLoggedIn) this.name = name
    isLoggedIn
  }

  def changePassword(newPass : String) = bankerAuth.changePassword(newPass)




    def getBalance(name : String):Int = {
   if (isLoggedIn){
     try {
       connection =  DriverManager.getConnection(CON_STRING)
       statement = connection.createStatement()
       val result  =statement.executeQuery("SELECT * FROM "+ LEDGER_TAB + s" WHERE user  = '$name'")
       val amount  = result.getInt(3)
       amount

     }
     catch {
       case e : Exception => println(e.getMessage)
         -1
     }
     finally {
       try {
         statement.close()
         connection.close()
       }
       catch {
         case e: Exception => println(e.getMessage)
       }
     }
   }
    else{
     println("Please logged In ")
     -1
   }

  }

  def viewAllAccountBalance = {

    if (isLoggedIn){
      try {
        connection =  DriverManager.getConnection(CON_STRING)
        statement = connection.createStatement()
        val result  =statement.executeQuery("SELECT * FROM "+ LEDGER_TAB )

//        println("NAME\tACCOUNT-TYPE\tBALANCE")
        printf("%10s %10s %10s\n","NAME","ACCOUNT_TYPE","BALANCE")
        while (result.next)
        {
          val name = result.getString(1)
          val acctype  =  result.getString(2)
          val balance  = result.getInt(3)
//          println(s"$name\t$acctype\t$balance")
          printf("%10s %10s %10d\n",name,acctype,balance)
        }


      }
      catch {
        case e : Exception => println(e.getMessage)

      }
      finally {
        try {
          statement.close()
          connection.close()
        }
        catch {
          case e: Exception => println(e.getMessage)
        }
      }


    }
    else{
      println("Please logIn first")
    }

  }

  def viewPassBook(name : String) = {
    if (isLoggedIn)
      {
        try {
          connection = DriverManager.getConnection(CON_STRING)
          statement = connection.createStatement()
          val passbook  = name + "PASSBOOK"
          val results  = statement.executeQuery("SELECT * FROM "+ passbook )

          printf("%10s %10s %10s %10s %10s\n","DATE","MODE","CREDIT","DEBIT","BALANCE")
          while (results.next)
          {
            val date  = results.getString(1)
            val mode  = results.getString(2)
            val credit = results.getInt(3)
            val debit = results.getInt(4)
            val amount = results.getInt(5)
            //          println(s"$date\t$mode\t\t$credit\t$debit\t$amount")
            printf("%10s %10s %10d %10d %10d \n" ,date,mode,credit,debit,amount)
          }
        }

        catch {
          case e: Exception => println(e.getMessage)

        }
        finally {
          try {
            statement.close()
            connection.close()
          }
          catch {
            case e : Exception => println(e.getMessage)
          }
        }
      }
    else{
      println("Please logged In")
    }
  }

}


//object Test {
//  def main(args: Array[String]): Unit = {
//
////    val banker = new BankerDB
//
////    banker.signUp("Bank","Pass")
////    banker.viewAllAccountBalance
////    banker.viewPassBook("Vikrant")
////    println(banker.getBalance("Vikrant"))
////    banker.changePassword("Bank","abcd")
////
////    banker.login("Bank","Pass")
////    banker.login("Bank","abcd")
//
//
//  }
//}
package Bank

import scala.io.StdIn
import Bank.Banker._
import Bank.User._

object Driver {


  def printMenu1  =  {
    println("Please enter your choice \n")
    println("1> SignUp\t 2>LogIn \t 3>Exit")
  }

  def printMenu2:Boolean = {
    while(true){
      println("Please enter your choice \n")
      println("Login as :\n")
      println("1>Banker \t 2>User \t3>Back \t4>Exit")
      val choice  =  StdIn.readInt()
      choice match {

        case 1 =>  {

        val bankerDB = new BankerDB
        val name =  StdIn.readLine("Input your name")
        val password  = StdIn.readLine("Input your password")
        if (bankerDB.login(name,password)) printBankerMenu(bankerDB)
        else println("Invalid credential")

        }
        case 2 => {


            val name = StdIn.readLine("Input your name")
            val password = StdIn.readLine("Input your password")
            val userDB = new UserDB
            if (userDB.login(name,password)) printUserMenu(userDB)
            else println("Invalid credential")




        }
        case 3 => return  true
        case 4 => sys.exit(0)
        case _ => println("Please input valid choice")

      }




    }
    true
  }

  def printBankerMenu(banker : BankerDB):Boolean = {
    while(true){
      println("\nPlease enter your choice\n")
      println("1>Change Password\t 2>Get Balance(User) \t3>Get All records \n4>View Passbook (User) \t5>Logout \t6>Exit")
      val choice = StdIn.readInt()
      if (choice == 1) {
          val password  = StdIn.readLine("Please enter your new password")
          banker.changePassword(password)
        }

      else if (choice ==2 ) {
        val name  =  StdIn.readLine("Please enter user's name")
        println(s"Avail balance : ${banker.getBalance(name)}")
      }

      else if (choice == 3) {
        banker.viewAllAccountBalance
        }

      else if (choice == 4) {
        val name  =  StdIn.readLine("Please enter user's name")
        banker.viewPassBook(name)

      }

      else if (choice == 5) {
          banker.logOut
          return true
        }

      else if (choice == 6){
        sys.exit(0)
      }

      else{
        println("Please enter valid input")
      }


    }
    return  true

  }




  def printUserMenu(userDB: UserDB):Boolean = {

    while (true) {
        println("\nPlease enter your choice\n")
        println("1>Create Account \t2>Apply for ATM \t3>Withdraw \n4>Deposit \t5>Check balance \t6>View Passbook\n7>Logout \t8>Exit")
        val choice  = StdIn.readInt()

        choice match  {
          case 1 =>{
                println("Please enter account type\n1>Savings \t2>Current")
                val ch = StdIn.readInt()
                if (ch==1) userDB.createAccount(AccountType.SAVINGS)
                else if (ch == 2) userDB.createAccount(AccountType.CURRENT)
                else println("please enter valid choice")
          }
          case 2 => {
            userDB.applyForAtm()
          }
          case 3 => {
            println("Please enter amount to withdraw")
            val amount  = StdIn.readInt()
            println("Please enter mode of transaction \n1>NETBANKING \t2>ATM")
            val mode  =  StdIn.readInt()
            if (mode == 1)userDB.withdraw(amount,Mode.NETBANKNG) else if (mode ==2 )userDB.withdraw(amount,Mode.ATM)
            else println("Please input valid choice")
          }
          case 4 => {
            println("Input amount to deposit")
            val amount = StdIn.readInt()
            userDB.deposit(amount)
          }
          case 5 =>{
            println(s"Your balance is  : ${userDB.checkBalance}")
          }

          case 6 => {
            userDB.viewPassBook
          }
          case 7 =>{
            userDB.logout()
            return true
          }
          case 8 =>{
            sys.exit(0)
          }
          case _ => println("Invalid Choice")

        }
      }

    true
  }

  def printMenu3:Boolean = {
    while(true){
      println("Please enter your choice \n")
      println("SignUp as :\n")
      println("1>Banker \t 2>User \t3>Back \t4>Exit")
      val choice  =  StdIn.readInt()
      if (choice == 1) {
          val banker = new BankerDB
          val name  =  StdIn.readLine("Please enter your name")
          val pass  = StdIn.readLine("Please enter your password")
          if (banker.signUp(name,pass)) printBankerMenu(banker)



      }
      else if (choice == 2){
        val userDB = new UserDB
        val name= StdIn.readLine("Please enter your name")
        val password  = StdIn.readLine("Please enter your prefer password")
        val phoneno = StdIn.readLine("Please enter your phone no")
        userDB.signUp(name,password,phoneno)
        printUserMenu(userDB)
      }
      else if (choice == 3) {
        return true
      }
      else if (choice == 4){
        System.exit(0)
      }
      else{
        println("Please enter valid input")
      }


    }
    true
  }


  def main(args: Array[String]): Unit = {
    while (true)
      {
        try{
          printMenu1
          val choice  = StdIn.readInt()
          if (choice  == 1) {
            printMenu3
          }
          else if (choice == 2) {
            printMenu2
          }

          else if (choice== 3) {
            System.exit(0)
          }
          else{
            println("Please enter valid input")
          }
        }
        catch {
          case e:Exception =>println(e.getMessage)
            println("Please enter valid input")
        }

      }
  }

}

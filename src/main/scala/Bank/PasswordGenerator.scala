package Bank

import scala.util.Random

class PasswordGenerator {
  val PASS_SIZE  = 20

  def generateRandomString(n : Int) : String = {

    if (n<=0) ""
    else Random.alphanumeric.take(n).toList.foldLeft(""){(a,b)  => a+ b }

  }

  def XorString(x:String,y:String)  = {
    val xorSeq = for (i <-  0 until x.length) yield{
      (x.charAt(i) ^ y.charAt(i))
    }
    xorSeq.mkString

  }

  def generateRandStr:String = {
    generateRandomString(PASS_SIZE)
  }


  def generateSaltedXorString(pass : String,salt : String) : String = {
    val newStr  = pass concat generateRandomString( PASS_SIZE -  pass.length)
//    println(newStr)
//    println(newStr.length)

    XorString(newStr,salt)
  }


  def getRandomPIN() :String = {
    val pin  = for (_ <- 1 to 4)yield Random.between(0,10)
    pin.mkString
  }

}

object Main {
  def main(args: Array[String]): Unit = {
    val generator = new PasswordGenerator
      println(generator.getRandomPIN)
  }
}
package Bank.User

import java.sql.{Connection, DriverManager, SQLException, Statement}
import java.text.SimpleDateFormat

import Bank.PasswordGenerator

class UserAuth {
  private val DB_NAME = "USER.db"
  private val CON_STRING  = "jdbc:sqlite://Users/apple/Desktop/scala/BankingSystem/" + DB_NAME
  private val ACC_TAB =  "account"
  private  val generator = new PasswordGenerator
  private var name  = ""
  private var phone = ""
  private var islogIn : Boolean = false
  private var isAtmAllocated :Boolean = false
  private var connection : Connection = null
  private var statement : Statement = null
  private val sdf = new SimpleDateFormat("dd-MM-yyyy")

  def isloggedIn :Boolean =  islogIn

  def getPhone : String = phone

  def getName  : String = this.name

  def login(user : String , password  : String) = {

    islogIn =if(validate(user, password)) true else false
    this.name  = user

    if (islogIn) println("Login Successfully!!!") else println("Invalid Credential. Try Again!!!")
    islogIn


  }

  def applyForAtm():String = {
    if (islogIn && !isAtmAllocated)
    {
      try {
        connection =  DriverManager.getConnection(CON_STRING)
        statement = connection.createStatement()
        statement.execute("CREATE TABLE IF NOT EXISTS atm (user text PRIMARY KEY NOT NULL," +
          " pin text )")

        val actual_pin = generator.getRandomPIN()
        val result = statement.executeQuery(s"SELECT * FROM atm where user = '${this.name}'")
        if (result.next()) {
          isAtmAllocated = true
          "Already allocated"
        }
        else{
          statement.execute(s"INSERT INTO atm(user,pin) VALUES('${this.name}' , '$actual_pin')")
          isAtmAllocated = true
          actual_pin

        }

      }
      catch {
        case e: Exception => println(e.getMessage)
          ""
      }

      finally{
        statement.close()
        connection.close()
      }

    }
    else{

      if (!isloggedIn) println("Please login first")
      else println("Already applied")
      ""
    }
  }

  def verify_pin(name : String, otp:String)  = {
    try {
      connection = DriverManager.getConnection(CON_STRING)
      statement = connection.createStatement()

      val result  = statement.executeQuery(s"SELECT * FROM atm WHERE user = '${this.name}'")
//      println(result)
      val actual_otp =  result.getString(2)
      actual_otp == otp
    }
    catch {
      case e : Exception => println(e.getMessage)
        false
    }
    finally {
      try{
        statement.close()
        connection.close()

      }
      catch {
        case e :Exception => println(e.getMessage)
      }
    }

  }


  def logout() = {
    if (islogIn) println("Successfully logout") else println("You are already logout")
    islogIn =false
    this.name =""
  }

  def signUp(name : String , pass : String,phone:String)  = {

    /*
    Logic   generate salt  with length of 20 -  pass.length
            generate rand_str with length 20
            xor both and generate password
     */

    try {
      connection =  DriverManager.getConnection(CON_STRING)
      statement = connection.createStatement()
      connection.setAutoCommit(false)
      statement.execute("CREATE TABLE IF NOT EXISTS " + ACC_TAB + " (name text PRIMARY KEY NOT NULL,phone text," +
        " salt text" +
        " , rand_str text , password text )")

      val salt = generator.generateRandomString(20 - pass.length)
      val newpass  = pass concat salt

      val rand_str  = generator.generateRandomString(20)

      val password =  generator.generateSaltedXorString(rand_str, newpass)

//      println("INSERT INTO "+ ACC_TAB + s" (name,phone, salt, rand_str, password)  VALUES ( '$name' , '$phone','$salt', '$rand_str', '$password' )")
      statement.execute("INSERT INTO "+ ACC_TAB + s" (name,phone, salt, rand_str, password)  VALUES ( '$name' ,'$phone', '$salt', '$rand_str', '$password' ) ")
      connection.commit()
      this.name =  name
      this.phone = phone
      islogIn = validate(name,pass)



    }
    catch {
      case e  : SQLException => println(e.getMessage)
    }

    finally {
      try {
        statement.close()
        connection.close()


      }
      catch {
        case e : Exception => println(e.getMessage)
      }
    }

  }

  def validate(username  : String , pass  : String) :Boolean  = {

    try {

      connection =  DriverManager.getConnection(CON_STRING)
      connection.setAutoCommit(false)
      statement  = connection.createStatement()

      val result =  statement.executeQuery(s"SELECT * FROM $ACC_TAB  WHERE name  = '$username'")

      val name  =  result.getString(1)
      val phone  = result.getString(2)
      val salt  =  result.getString(3)
      val rand_str = result.getString(4)
      val password = result.getString(5)

      val newPass =  pass + salt


      val expectedPass = generator.XorString(newPass.take(20), rand_str)
      //      println(result)
      connection.commit()
      if (expectedPass ==  password) true  else false

    }
    catch {
      case e : Exception =>println(e.getMessage)
        false
    }
    finally{
      try {
        statement.close()
        connection.close()

      }
      catch {
        case e: Exception => println(e.getMessage)
      }
    }
  }
  def generateOTP : String  =  generator.getRandomPIN()






}

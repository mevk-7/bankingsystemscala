package Bank.User

import java.sql.{Connection, DriverManager, Statement}
import java.text.SimpleDateFormat
import java.util.Date

import Bank.{AccountType, Mode}

import scala.io.StdIn



class UserDB{
  private val DB_NAME = "USER.db"
  private val CON_STRING  = "jdbc:sqlite://Users/apple/Desktop/scala/BankingSystem/" + DB_NAME
  private val LEDGER_TAB = "ledger"
  private var name  = ""
  private var connection : Connection = null
  private var statement : Statement = null
  private val sdf = new SimpleDateFormat("dd-MM-yyyy")
  private val userAuth = new UserAuth

  def login(user : String , password  : String) = {

    userAuth.login(user, password)
    this.name  = if (userAuth.isloggedIn) user else ""
    userAuth.isloggedIn
  }
  def applyForAtm():String = {
    val pin = userAuth.applyForAtm()
    println(s"Your atm pin is : $pin")
    pin
  }
  def logout() = userAuth.logout()
  def signUp(name : String , pass : String,phone:String)  = {

    userAuth.signUp(name, pass, phone)
    this.name  =if (userAuth.isloggedIn) name else ""
    userAuth.isloggedIn
  }




    def createAccount(account_type: AccountType.Value) ={

    if (userAuth.isloggedIn){


      try{
        connection = DriverManager.getConnection(CON_STRING)
        statement = connection.createStatement()
        connection.setAutoCommit(false)
        val passbook  =  s"${this.name}"+"PASSBOOK"
        statement.execute( s"CREATE TABLE IF NOT EXISTS " + passbook+ " (date text, mode text, " +
          s" credit INTEGER," +
          s" debit INTEGER, amount INTEGER)")



        statement.execute(s"CREATE TABLE IF NOT EXISTS "+ LEDGER_TAB+ " (user text PRIMARY KEY, type text, amount INTEGER) ")
        connection.commit()


        val date = new Date

//        println(s"INSERT INTO " + passbook+" (date,mode, credit , debit, amount) VALUES (" +       s" ' ${sdf.format(date)}','bank', 0, 0 , 0")

        statement.execute(s"INSERT INTO " + passbook+" (date,mode, credit , debit, amount) VALUES (" +
          s" '${sdf.format(date)}','BANK', 0, 0 , 0)")
        statement.execute(s"INSERT INTO "+ LEDGER_TAB+ s"(user,type,amount) VALUES('${this.name}', '${account_type.toString}' , 0)")
        connection.commit()




      }
      catch {
        case e : Exception => println(e.getMessage)
      }
      finally {
        try {
          statement.close()
          connection.close()

        }
        catch {
          case e: Exception => println(e.getMessage)
        }
      }

    }
    else {
      println("Please log In / Sign up")
    }
  }

  def validOTP  = {
    println(s"\nSending OTP to your no  ${userAuth.getPhone}")
    val otp = userAuth.generateOTP
    println(s"OTP  : $otp")
    val otp2  =  StdIn.readLine("Please enter your otp : ")
    if (otp == otp2) true else false

  }

  def withdraw(value :  Int,mode : Mode.Value ,name :String =""):Boolean = {
    /*
    Debited in bank account
    Logic - current account have negative balance
          - but savings account cannot have negative balance
     */

    if ((!userAuth.isloggedIn && mode.toString == "NETBANKING") || value <= 0)
      {
        if (value <= 0 )
          println(s"Invalid amount $value" )

        else
          println("Please logged in !!!")

        false
      }

    else{
      var verify = false

      if (mode.toString == "NETBANKING")
        {
          verify = validOTP
        }
      else if (mode.toString =="ATM")
        {
          val otp =  StdIn.readLine("Please input your PIN")
//          val name  = StdIn.readLine("Please input your name")
          verify = userAuth.verify_pin(name, otp)
          if (verify) this.name = userAuth.getName else println("Invalid PIN pleases try again")


        }
      if (verify){
        try {


          connection = DriverManager.getConnection(CON_STRING)
          statement = connection.createStatement()
          connection.setAutoCommit(false)
          val result  =  statement.executeQuery(s"SELECT * FROM  " + LEDGER_TAB+ s" WHERE user  = '${this.name}' ")
          val user  = result.getString(1)
          val account_type  = result.getString(2)
          val balance  = result.getInt(3)
          val passbook = s"${this.name}"+"PASSBOOK"
          if (account_type == "Current")
          {
            println(s"Debited amount : $value")
            statement.execute(s"UPDATE "+ LEDGER_TAB+  s" SET amount = ${balance - value} WHERE user = '${this.name}' ")
            val date  = new Date

            statement.execute(s"INSERT INTO "+passbook+" (date,mode," +
              s"credit,debit,amount) VALUES ('${this.sdf.format(date)}', '${mode.toString}'" +
              s", 0 , $value, ${balance-value})")

            connection.commit()
            true
          }
          else if (account_type == "Savings"){
            if (balance <  value)
            {
              println(s"Cannot debit amount $value . Current balance  : $balance")
              false
            }
            else{
              statement.execute(s"UPDATE " + LEDGER_TAB+  s" SET amount = ${balance - value} WHERE user = '${this.name}' ")
              val date  = new Date

              statement.execute(s"INSERT INTO " + passbook +" (date,mode,credit,debit,amount)" +
                s" VALUES ('${this.sdf.format(date)}' , '${mode.toString}'" +
                s", 0 , $value, ${balance-value})")

              connection.commit()
              true
            }


          }
          else{
            false
          }

        }
        catch {
          case e :  Exception => println(e.getMessage)
            false
        }
        finally{
          try {
            statement.close()
            connection.close()
          }
          catch {
            case e : Exception => println(e.getMessage)

          }
        }
      }
      else{
        false
      }

    }

  }

  def deposit(value : Int) : Boolean  = {
    /*
    Debited
     */

    if (value <=0 || !userAuth.isloggedIn)
      {
        if (!userAuth.isloggedIn) println("Please login") else println(s"Invalid amount $value")
        false
      }
    else{
      try {
        connection =  DriverManager.getConnection(CON_STRING)
        statement =  connection.createStatement()
        connection.setAutoCommit(false)
        val passbook = s"${this.name}" +"PASSBOOK"
        val result  =  statement.executeQuery(s"SELECT * FROM " +LEDGER_TAB +s" WHERE user  = '${this.name}' ")
//        val name  = result.getString(1)
//        val account_type  = result.getString(2)
        val balance  =  result.getInt(3)
        statement.execute(s"UPDATE " +LEDGER_TAB+ s" SET amount  = ${balance+value} WHERE user = '${this.name}'")
        val date = new Date
        statement.execute(s"INSERT INTO "+ passbook + " (date,mode,credit,debit,amount) VALUES" +
          s" ('${this.sdf.format(date)}', 'BANK'" +
          s", $value , 0, ${balance+value})")

        connection.commit()
        true
      }
      catch {
        case e : Exception => println(e.getMessage)
          false
      }
      finally {
        try {
          statement.close()
          connection.close()

        }
        catch {
          case e : Exception => println(e.getMessage)
        }
      }
    }

  }

  def viewPassBook ={
    if (!userAuth.isloggedIn)
      {
        println("Please logged in")
      }
    else{
      try {
        connection = DriverManager.getConnection(CON_STRING)
        statement = connection.createStatement()
        val  results = statement.executeQuery(s"SELECT * FROM ${this.name}PASSBOOK")
        /*
        date,mode,credit,debit,amount
         */
//        println(s"DATE\t\tMODE\tCREDIT\tDEBIT\tBALANCE")
        printf("%10s %10s %10s %10s %10s\n","DATE","MODE","CREDIT","DEBIT","BALANCE")
        while (results.next)
        {
          val date  = results.getString(1)
          val mode  = results.getString(2)
          val credit = results.getInt(3)
          val debit = results.getInt(4)
          val amount = results.getInt(5)
//          println(s"$date\t$mode\t\t$credit\t$debit\t$amount")
          printf("%10s %10s %10d %10d %10d \n" ,date,mode,credit,debit,amount)
        }

      }
      catch {
        case e: Exception => println(e.getMessage)

      }
      finally {
        try{
          statement.close()
          connection.close()

        }
        catch {
          case e: Exception => println(e.getMessage)
        }
      }
    }

  }

  def checkBalance = {
    if (!userAuth.isloggedIn)
      {
        println("please logged in")
      }
    else{
      try {
        connection  = DriverManager.getConnection(CON_STRING)
        statement = connection.createStatement()
        val result  = statement.executeQuery("SELECT * FROM  "+LEDGER_TAB + s" WHERE user = '${this.name}'")
        println(s"Available balance is : ${result.getInt(3)}")
        result.getInt(3)

      }
      catch {
        case e : Exception => println(e.getMessage)
        -1
      }
      finally {
        try {
          statement.close()
          connection.close()
        }
        catch {
          case e : Exception => println(e.getMessage)
        }
      }
    }
  }


}

//object MainTest {
//
//  def main(args: Array[String]): Unit = {
//
//    val userDB = new UserDB
//    userDB.signUp("Vikrant","pass","9110199559")
//    userDB.createAccount(AccountType.SAVINGS)
//    userDB.deposit(2000)
//    userDB.applyForAtm()
//    userDB.withdraw(1000,mode=Mode.ATM,"Vikrant")
//
//    userDB.withdraw(300, Mode.NETBANKNG)
//    userDB.deposit(5000)
//    println(userDB.checkBalance)
//
//    userDB.viewPassBook
//
//
//
//
//
//
//  }
//}